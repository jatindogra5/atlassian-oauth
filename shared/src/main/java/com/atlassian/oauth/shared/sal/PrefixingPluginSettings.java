package com.atlassian.oauth.shared.sal;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

import static com.google.common.base.Preconditions.checkNotNull;

public class PrefixingPluginSettings implements PluginSettings {
    private final PluginSettings settings;
    private final String prefix;

    public PrefixingPluginSettings(PluginSettings settings, String prefix) {
        this.settings = checkNotNull(settings, "settings");
        this.prefix = checkNotNull(prefix, "prefix");
    }

    public Object get(String key) {
        return settings.get(prefix + "." + key);
    }

    public Object put(String key, Object value) {
        return settings.put(prefix + "." + key, value);
    }

    public Object remove(String key) {
        return settings.remove(prefix + "." + key);
    }
}

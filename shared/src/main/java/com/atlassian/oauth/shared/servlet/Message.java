package com.atlassian.oauth.shared.servlet;

import com.atlassian.sal.api.message.I18nResolver;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

public final class Message {
    private final String key;
    private final Serializable[] params;
    private final I18nResolver resolver;

    public Message(I18nResolver resolver, String key, Serializable[] params) {
        this.resolver = checkNotNull(resolver, "resolver");
        this.key = checkNotNull(key, "key");
        this.params = checkNotNull(params, "params");
    }

    @Override
    public String toString() {
        return resolver.getText(key, params);
    }
}

package com.atlassian.oauth;

import net.jcip.annotations.Immutable;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.net.URI;
import java.security.PublicKey;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * <p>Immutable representation of an OAuth consumer.  At a miminum a consumer is required to have a key, a name, and a
 * public key.  A consumer can also be configured to have a description and a default callback URL that will be used
 * if a callback URL is not provided after a user authorizes a request token.</p>
 *
 * <p>Instances of {@code Consumer} objects can be constructed using the builder. For example</p>
 * <pre>
 *   Consumer c = Consumer.key("consumer-key").name("Consumer").publicKey(publicRSAKey).build();
 * </pre>
 */
@Immutable
public final class Consumer {
    private final String key;
    private final String name;
    private final String description;
    private final SignatureMethod signatureMethod;
    private final PublicKey publicKey;
    private final URI callback;

    private final boolean threeLOAllowed;
    private final boolean twoLOAllowed;
    private final String executingTwoLOUser;
    private final boolean twoLOImpersonationAllowed;

    private Consumer(InstanceBuilder builder) {
        key = builder.key;
        name = builder.name;
        signatureMethod = builder.signatureMethod;
        publicKey = builder.publicKey;
        description = builder.description;
        callback = builder.callback;
        threeLOAllowed = builder.threeLOAllowed;
        twoLOAllowed = builder.twoLOAllowed;
        executingTwoLOUser = builder.executingTwoLOUser;
        twoLOImpersonationAllowed = builder.twoLOImpersonationAllowed;
    }

    /**
     * Static factory method that starts the process of building a {@code Consumer} instance.  Returns an
     * {@code InstanceBuilder} so the other attribute can be set.
     *
     * @param key unique key used to identify the consumer in requests unauthorized OAuth request tokens
     * @return the builder for constructing the rest of the {@code Consumer} instance
     */
    public static InstanceBuilder key(String key) {
        return new InstanceBuilder(checkNotNull(key, "key"));
    }

    /**
     * Returns the unique key used to identify the consumer in requests unauthorized OAuth request tokens.
     *
     * @return the unique key used to identify the consumer in requests unauthorized OAuth request tokens
     */
    public String getKey() {
        return key;
    }

    /**
     * Returns the name of the consumer as it will be displayed to the user.
     *
     * @return the name of the consumer as it will be displayed to the user
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the method the consumer uses to sign requests.
     *
     * @return the method the consumer uses to sign requests
     */
    public SignatureMethod getSignatureMethod() {
        return signatureMethod;
    }

    /**
     * Returns the RSA public key for the consumer, {@code null} if the signature method is not RSA-SHA1.
     * If the signature method is RSA-SHA1, this key is used in verifying the signature in requests from the consumer.
     *
     * @return the RSA public key for the consumer, {@code null} if the signature method is not RSA-SHA1
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * Returns the optional description of the consumer as it would be displayed to the user, {@code null} if the
     * description was not set.
     *
     * @return the optional description of the consumer as it would be displayed to the user, {@code null} if the
     * description was not set.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the default callback {@code URI} used after a request token has been authorized if no callback
     * {@code URI} was provided in the authorization request.
     *
     * @return the default callback {@code URI} used after a request token has been authorized
     */
    public URI getCallback() {
        return callback;
    }

    /**
     * Returns whether 3 Legged OAuth requests are allowed for this consumer.
     *
     * @return whether 3 Legged OAuth requests are allowed for this consumer
     */
    public boolean getThreeLOAllowed() {
        return threeLOAllowed;
    }

    /**
     * Returns whether 2 Legged OAuth requests are allowed for this consumer.
     *
     * @return whether 2 Legged OAuth requests are allowed for this consumer
     */
    public boolean getTwoLOAllowed() {
        return twoLOAllowed;
    }

    /**
     * Returns the user whose credential is used to execute 2LO requests.
     * This could be null only if {@link #twoLOAllowed} is false.
     *
     * @return the user whose credential is used to execute 2LO requests
     */
    public String getExecutingTwoLOUser() {
        return executingTwoLOUser;
    }

    /**
     * Returns whether 2 Legged OAuth with impersonation requests are allowed for this consumer.
     *
     * @return whether 2 Legged OAuth with impersonation requests are allowed for this consumer
     */
    public boolean getTwoLOImpersonationAllowed() {
        return twoLOImpersonationAllowed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("key", key)
                .append("name", name)
                .append("description", description)
                .append("callback", callback)
                .append("signatureMethod", signatureMethod)
                .append("publicKey", publicKey)
                .append("threeLOAllowed", threeLOAllowed)
                .append("twoLOAllowed", twoLOAllowed)
                .append("executingTwoLOUser", executingTwoLOUser)
                .append("twoLOImpersonationAllowed", twoLOImpersonationAllowed)
                .toString();
    }

    /**
     * The ways in which consumers can sign requests, as per
     * @see <a href="http://oauth.net/core/1.0/#signing_process">OAuth spec, Section 9</a>
     */
    public enum SignatureMethod {
        HMAC_SHA1, RSA_SHA1
    }

    /**
     * Builder allowing the optional attributes of the {@code Consumer} object under construction to be set and
     * construction of the final {@code Consumer} instance.
     *
     * By default a newly created Consumer will have 3LO enabled unless {@link InstanceBuilder#threeLOAllowed(boolean)}
     * with 'false' value is called.
     */
    public static final class InstanceBuilder {
        private final String key;

        private String name;
        private SignatureMethod signatureMethod;
        private PublicKey publicKey;
        private String description;
        private URI callback;

        private boolean twoLOAllowed;
        private String executingTwoLOUser;
        private boolean twoLOImpersonationAllowed;
        private boolean threeLOAllowed;

        public InstanceBuilder(String key) {
            this.key = key;
            // For API backward compatibility, not calling {@link #threeLOAllowed} would result in 3LO being allowed.
            this.threeLOAllowed = true;
        }

        /**
         * Sets the {@code name} attribute of the {@code Consumer} object under construction and returns {@code this}
         * builder to allow other attributes to be set
         *
         * @param name value to be used as the {@code name} attribute of the {@code Consumer} being constructed
         * @return {@code this} builder
         */
        public InstanceBuilder name(String name) {
            this.name = checkNotNull(name);
            return this;
        }

        /**
         * Sets the {@code signatureMethod} attribute of the {@code Consumer} object under construction and returns
         * {@code this} builder to allow other attributes to be set
         *
         * @param signatureMethod {@code SignatureMethod} to be used when signing requests as this consumer
         * @return {@code this} builder
         */
        public InstanceBuilder signatureMethod(SignatureMethod signatureMethod) {
            this.signatureMethod = checkNotNull(signatureMethod);
            return this;
        }

        /**
         * Sets the {@code publicKey} attribute of the {@code Consumer} object under construction and returns
         * {@code this} builder to allow other attributes to be set
         *
         * @param publicKey RSA {@code PublicKey} to be used as the {@code publicKey} attribute of the {@code Consumer}
         *                  being constructed
         * @return {@code this} builder
         */
        public InstanceBuilder publicKey(PublicKey publicKey) {
            this.signatureMethod = SignatureMethod.RSA_SHA1;
            this.publicKey = checkNotNull(publicKey);
            return this;
        }

        /**
         * Sets the description of the consumer as it would be displayed to the user and returns
         * {@code this} builder to allow other attributes to be set
         *
         * @param description the description of the consumer as it would be displayed to the user
         * @return {@code this} builder
         */
        public InstanceBuilder description(String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the default callback URL used after a request token has been authorized and returns
         * {@code this} builder to allow other attributes to be set
         *
         * @param callback the default callback URI used after a request token has been authorized
         * @return {@code this} builder
         */
        public InstanceBuilder callback(URI callback) {
            this.callback = callback;
            return this;
        }

        /**
         * Sets whether to allow 3LO requests from consumers
         * {@code this} builder to allow other attributes to be set
         *
         * @param threeLOAllowed whether 3LO requests from the consumer are allowed
         * @return {@code this} builder
         */
        public InstanceBuilder threeLOAllowed(boolean threeLOAllowed) {
            this.threeLOAllowed = threeLOAllowed;
            return this;
        }

        /**
         * Sets whether to allow 2LO requests from consumers
         * {@code this} builder to allow other attributes to be set
         *
         * @param twoLOAllowed whether 2LO requests from the consumer are allowed
         * @return {@code this} builder
         */
        public InstanceBuilder twoLOAllowed(boolean twoLOAllowed) {
            this.twoLOAllowed = twoLOAllowed;
            return this;
        }

        /**
         * Sets the username for 2LO requests to be executed as
         * {@code this} builder to allow other attributes to be set
         *
         * @param executeAsUser the username for 2LO requests to be executed as
         * @return {@code this} builder
         */
        public InstanceBuilder executingTwoLOUser(String executeAsUser) {
            this.executingTwoLOUser = executeAsUser;
            return this;
        }

        /**
         * Sets whether to allow 2LO requests to impersonate *any* user on the system
         * Normally this option would be on only for requests from fully-trusted consumers.
         * {@code this} builder to allow other attributes to be set
         *
         * @param twoLOImpersonationAllowed whether to allow 2LO requests to impersonate any user on the system
         * @return {@code this} builder
         */
        public InstanceBuilder twoLOImpersonationAllowed(boolean twoLOImpersonationAllowed) {
            this.twoLOImpersonationAllowed = twoLOImpersonationAllowed;
            return this;
        }

        /**
         * Constructs and returns the final{@code Consumer} instance.
         *
         * @return the final {@code Consumer} instance
         */
        public Consumer build() {
            checkNotNull(name, "name");
            checkNotNull(signatureMethod, "signatureMethod");
            if (signatureMethod == SignatureMethod.RSA_SHA1) {
                checkNotNull(publicKey, "publicKey must be set when the signature method is RSA-SHA1");
            }
            return new Consumer(this);
        }
    }
}

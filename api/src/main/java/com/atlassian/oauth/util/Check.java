package com.atlassian.oauth.util;

import com.google.common.base.Preconditions;

/**
 * Utility to perform checks on parameters.
 */
public final class Check {
    /**
     * This is here for Atlassian Gadgets until AG-1417 is deployed.
     *
     * @deprecated use {@link Preconditions#checkNotNull(Object, Object)}
     */
    public static <T> T notNull(T reference, Object errorMessage) {
        return Preconditions.checkNotNull(reference, errorMessage);
    }

    /**
     * Checks if the string is {@code null}, empty, or contains only whitespace.
     *
     * @param str          {@code String} to check
     * @param errorMessage to be used as a message in case there's an exception thrown
     * @return {@code str} so it may be used
     * @throws NullPointerException     if {@code str} is {@code null}
     * @throws IllegalArgumentException if {@code str} is empty or contains only whitespace
     */
    public static String notBlank(String str, Object errorMessage) {
        Preconditions.checkNotNull(str, errorMessage);
        Preconditions.checkArgument(!str.isEmpty() && !str.trim().isEmpty(), String.valueOf(errorMessage));
        return str;
    }
}

# OAuth Plugin

## Description

Atlassian applications house a large quantity of data that is used in the day to day lives of developers, managers and other stakeholders involved in developing software. Right now, the only place they can access that data is from within Atlassian applications. To allow them to access it from 3rd party sites, such as iGoogle or GMail, they need to be able to authorize these 3rd parties to access their data on their behalf. For some of these users, Atlassian products like JIRA are their main destination on the web. For JIRA to be able to pull information from 3rd party applications, JIRA and it's plugins need a way to request access and gain authorization to use the users data from 3rd party applications. OAuth provides a means of allowing the user to delegate authorization to access their data. Our goal is to allow Atlassian applications to act as both Service Providers and Consumers.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.
The main active branches are:

- [atlassian-plugins-1.2.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.2.x): Bug fixes for products supporting Platform 2.9.0 - 2.12.0. Topic branches here should be prefixed with issue-1.2/.
- [atlassian-plugins-1.3.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.3.x): Bug fixes for products supporting Platform 2.13.0. Topic branches here should be prefixed with issue-1.3/.
- [atlassian-plugins-1.4.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.4.x): Bug fixes for products supporting Platform 2.14.0 - 2.15.0. Topic branches here should be prefixed with issue-1.4/.
- [atlassian-plugins-1.5.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.5.x): Bug fixes for products supporting Platform 2.16.0. Topic branches here should be prefixed with issue-1.5/.
- [atlassian-plugins-1.6.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.6.x): Bug fixes for products supporting Platform 2.17.0. Topic branches here should be prefixed with issue-1.6/.
- [atlassian-plugins-1.7.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.7.x): Bug fixes for products supporting Platform 2.18.0. Topic branches here should be prefixed with issue-1.7/.
- [atlassian-plugins-1.8.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.8.x): Bug fixes for products supporting Platform 2.19.0 - 2.20.0. Topic branches here should be prefixed with issue-1.8/.
- [atlassian-plugins-1.9.x](https://bitbucket.org/atlassian/atlassian-oauth/branch/atlassian-oauth-1.9.x): New features for the next backwards compatible release of Atlassian OAuth. Topic branches here should be prefixed with issue-1.9/.
- [master](https://bitbucket.org/atlassian/atlassian-oauth/branch/master): New features and breaking changes working towards oauth 2.0.x Topic branches here should be prefixed with issue/.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/OAUTH)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/OAUTH)

### Documentation

[OAuth Documentation](https://ecosystem.atlassian.net/wiki/display/OAUTH)

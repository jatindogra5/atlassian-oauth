package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Displays the authorization form
 */
final class GetAuthorizationPage implements AuthorizationRequestProcessor {
    private final AuthorizationRenderer renderer;

    public GetAuthorizationPage(AuthorizationRenderer renderer) {
        this.renderer = checkNotNull(renderer, "renderer");
    }

    public void process(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException {
        renderer.render(request, response, token);
    }
}
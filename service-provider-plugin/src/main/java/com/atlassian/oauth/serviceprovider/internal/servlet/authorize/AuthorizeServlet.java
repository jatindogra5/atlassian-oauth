package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.internal.servlet.OAuthProblemUtils;
import com.atlassian.oauth.shared.servlet.ResponseHeaderUtil;
import com.atlassian.oauth.serviceprovider.internal.servlet.TokenLoader;
import com.atlassian.oauth.serviceprovider.internal.servlet.TransactionalServlet;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;
import net.oauth.OAuthException;
import net.oauth.OAuthProblemException;
import net.oauth.server.OAuthServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static net.oauth.OAuth.Problems.TOKEN_USED;
import static net.oauth.server.OAuthServlet.handleException;

@SuppressWarnings("serial")
public final class AuthorizeServlet extends TransactionalServlet {
    static final int VERIFIER_LENGTH = 6;

    private static final String AUTH_ERROR_TEMPLATE = "templates/auth/authorize-error.vm";

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final AuthorizationRequestProcessor get;
    private final AuthorizationRequestProcessor post;
    private final TokenLoader loader;
    private final LoginRedirector loginRedirector;
    private final ApplicationProperties applicationProperties;
    private final TemplateRenderer templateRenderer;
    private final I18nResolver i18nResolver;

    public AuthorizeServlet(@Qualifier("getAuthorizationProcessor") AuthorizationRequestProcessor get,
                            @Qualifier("postAuthorizationProcessor") AuthorizationRequestProcessor post,
                            TokenLoader loader,
                            LoginRedirector loginRedirector,
                            ApplicationProperties applicationProperties,
                            TransactionTemplate transactionTemplate,
                            TemplateRenderer templateRenderer,
                            I18nResolver i18nResolver) {
        super(transactionTemplate);
        this.get = checkNotNull(get, "get");
        this.post = checkNotNull(post, "post");
        this.loader = checkNotNull(loader, "loader");
        this.loginRedirector = checkNotNull(loginRedirector, "loginRedirector");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    @Override
    public void doGetInTransaction(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        process(get, request, response);
        ResponseHeaderUtil.preventCrossFrameClickJacking(response);
    }

    @Override
    public void doPostInTransaction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        process(post, request, response);
        ResponseHeaderUtil.preventCrossFrameClickJacking(response);
    }

    private void process(AuthorizationRequestProcessor processor, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ServiceProviderToken token;
        try {
            token = loader.getTokenForAuthorization(request);
        } catch (OAuthException e) {
            if (e instanceof OAuthProblemException) {
                OAuthProblemUtils.logOAuthProblem(OAuthServlet.getMessage(request, null), (OAuthProblemException) e, log);
            }
            handleException(response, e, applicationProperties.getBaseUrl(), false);
            // setting content type directly to "text/html;charset=UTF-8" does not seem to work,
            //  probably due to the response.reset() call in handleException() above
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");

            ImmutableMap.Builder<String, Object> contextBuilder = ImmutableMap.builder();
            contextBuilder.put("message", getMessage(e));
            contextBuilder.put("applicationProperties", applicationProperties);

            templateRenderer.render(AUTH_ERROR_TEMPLATE, contextBuilder.build(), response.getWriter());
            return;
        }

        if (!loginRedirector.isLoggedIn(request)) {
            loginRedirector.redirectToLogin(request, response);
        } else {
            processor.process(request, response, token);
        }
    }

    private String getMessage(OAuthException e) {
        if (e instanceof OAuthProblemException) {
            OAuthProblemException problem = (OAuthProblemException) e;
            if (TOKEN_REJECTED.equals(problem.getProblem())) {
                return i18nResolver.getText("com.atlassian.oauth.serviceprovider.authorize.error.token.rejected");
            } else if (TOKEN_USED.equals(problem.getProblem())) {
                return i18nResolver.getText("com.atlassian.oauth.serviceprovider.authorize.error.token.used");
            } else if (TOKEN_EXPIRED.equals(problem.getProblem())) {
                return i18nResolver.getText("com.atlassian.oauth.serviceprovider.authorize.error.token.expired");
            }
        }
        return i18nResolver.getText("com.atlassian.oauth.serviceprovider.authorize.error.generic");
    }
}

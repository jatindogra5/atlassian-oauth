package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.OAuthProblem.Problem;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.auth.Authenticator.Result;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserResolutionException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_2LO;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_2LO_BUT_BLANK_EXECUTING_USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_2LO_BUT_NO_EXECUTING_USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_2LO_IMPERSONATION;
import static com.atlassian.oauth.testing.TestData.LONG_USERNAME;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.USERNAME;
import static com.atlassian.oauth.testing.TestData.USER_WITH_LONG_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticatorImplTest {
    private static final String OAUTH_CONSUMER_KEY = "com.atlassian.oath.consumer-key";
    private static final String TOKEN = "1234";
    private static final ServiceProviderToken REQUEST_TOKEN = ServiceProviderToken.newRequestToken(TOKEN)
            .tokenSecret("5678")
            .consumer(RSA_CONSUMER)
            .version(Version.V_1_0_A)
            .build();
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN)
            .tokenSecret("5678")
            .consumer(RSA_CONSUMER)
            .authorizedBy(USER)
            .build();
    private static final ServiceProviderToken EXPIRED_ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN)
            .tokenSecret("5678")
            .consumer(RSA_CONSUMER)
            .authorizedBy(USER)
            .creationTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_ACCESS_TOKEN_TTL * 2)
            .build();
    // This token is very old but not yet expired.
    private static final ServiceProviderToken FOUR_YEARS_OLD_ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN)
            .tokenSecret("5678")
            .consumer(RSA_CONSUMER)
            .authorizedBy(USER)
            .creationTime(System.currentTimeMillis() - 4 * 365 * 24 * 60 * 60 * 1000L)
            .build();


    @Mock
    ServiceProviderTokenStore store;
    @Mock
    OAuthValidator validator;
    @Mock
    AuthenticationController authenticationController;
    @Mock
    ApplicationProperties applicationProperties;

    @Mock
    Clock clock;
    @Mock
    ServiceProviderConsumerStore serviceProviderConsumerStore;
    @Mock
    UserManager userManager;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    TransactionTemplate transactionTemplate;
    OAuthConverter converter;
    Authenticator authenticator;

    Map<String, String[]> rsaConsumerParameterMap;
    Map<String, String[]> rsaConsumerWith2LOParameterMap;
    Map<String, String[]> rsaConsumerWith2LOButNoExecutingUserParameterMap;
    Map<String, String[]> rsaConsumerWith2LOButBlankExecutingUserParameterMap;
    Map<String, String[]> rsaConsumerWith2LOImpersonationParameterMap;
    ByteArrayOutputStream responseOutputStream;

    @Before
    public void setUp() throws IOException {
        rsaConsumerParameterMap = Maps.newHashMap();
        rsaConsumerParameterMap.put("oauth_token", new String[]{TOKEN});
        rsaConsumerParameterMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER.getKey()});
        rsaConsumerParameterMap.put("oauth_signature_method", new String[]{"PLAINTEXT"});
        rsaConsumerParameterMap.put("oauth_signature", new String[]{"&"});
        rsaConsumerParameterMap.put("oauth_timestamp", new String[]{Long.toString(System.currentTimeMillis() / 1000L)});
        rsaConsumerParameterMap.put("oauth_nonce", new String[]{"oauth_nonce"});

        rsaConsumerWith2LOParameterMap = Maps.newHashMap(rsaConsumerParameterMap);
        rsaConsumerWith2LOParameterMap.put("oauth_token", new String[]{TOKEN});
        rsaConsumerWith2LOParameterMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER_WITH_2LO.getKey()});

        rsaConsumerWith2LOButNoExecutingUserParameterMap = Maps.newHashMap(rsaConsumerParameterMap);
        rsaConsumerWith2LOButNoExecutingUserParameterMap.put("oauth_token", new String[]{TOKEN});
        rsaConsumerWith2LOButNoExecutingUserParameterMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER_WITH_2LO_BUT_NO_EXECUTING_USER.getKey()});

        rsaConsumerWith2LOButBlankExecutingUserParameterMap = Maps.newHashMap(rsaConsumerParameterMap);
        rsaConsumerWith2LOButBlankExecutingUserParameterMap.put("oauth_token", new String[]{TOKEN});
        rsaConsumerWith2LOButBlankExecutingUserParameterMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER_WITH_2LO_BUT_BLANK_EXECUTING_USER.getKey()});

        rsaConsumerWith2LOImpersonationParameterMap = Maps.newHashMap(rsaConsumerParameterMap);
        rsaConsumerWith2LOImpersonationParameterMap.put("oauth_token", new String[]{TOKEN});
        rsaConsumerWith2LOImpersonationParameterMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey()});

        when(applicationProperties.getBaseUrl()).thenReturn("http://host");
        converter = new OAuthConverter(new ServiceProviderFactoryImpl(applicationProperties));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://host/service"));
        when(request.getRequestURI()).thenReturn("/service");
        when(request.getMethod()).thenReturn("GET");

        responseOutputStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseOutputStream));
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());

        when(serviceProviderConsumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(serviceProviderConsumerStore.get(RSA_CONSUMER_WITH_2LO.getKey())).thenReturn(RSA_CONSUMER_WITH_2LO);
        when(serviceProviderConsumerStore.get(RSA_CONSUMER_WITH_2LO_BUT_NO_EXECUTING_USER.getKey())).thenReturn(RSA_CONSUMER_WITH_2LO_BUT_NO_EXECUTING_USER);
        when(serviceProviderConsumerStore.get(RSA_CONSUMER_WITH_2LO_BUT_BLANK_EXECUTING_USER.getKey())).thenReturn(RSA_CONSUMER_WITH_2LO_BUT_BLANK_EXECUTING_USER);
        when(serviceProviderConsumerStore.get(RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey())).thenReturn(RSA_CONSUMER_WITH_2LO_IMPERSONATION);

        transactionTemplate = new PassThruTransactionTemplate();
        authenticator = new AuthenticatorImpl(store, validator, converter, authenticationController, transactionTemplate,
                applicationProperties, clock, serviceProviderConsumerStore, userManager);

        when(userManager.resolve(USER.getName())).thenReturn(USER);
        when(userManager.resolve("")).thenThrow(new UserResolutionException(""));
    }

    @Test
    public void assertThatFailureResultWhenTheConsumerNoLongerExists() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(serviceProviderConsumerStore.get(RSA_CONSUMER.getKey())).thenReturn(null);
        Result failure = new Result.Failure(new OAuthProblem(Problem.CONSUMER_KEY_UNKNOWN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(request, never()).setAttribute(anyString(), anyString());
    }

    @Test
    public void assertThatSuccessIsReturnedForValidAccessTokenWhenUserCanLogIn() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(USER);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
        verify(request).setAttribute(OAUTH_CONSUMER_KEY, RSA_CONSUMER.getKey());
    }

    @Test
    public void assertThatFailureResultForValidAccessTokenButConsumerKeyDoesNotMatch() {
        Map<String, String[]> paramMap = new HashMap<String, String[]>(rsaConsumerParameterMap);
        paramMap.put("oauth_consumer_key", new String[]{RSA_CONSUMER_WITH_2LO.getKey()});
        setupRequestWithParameters(paramMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        Result failure = new Result.Failure(new OAuthProblem(Problem.TOKEN_REJECTED, TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
    }

    @Test
    public void assertThatFailureResultWithInvalidTokenMessageIsReturnedWhenTokenDoesNotExist() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        Result failure = new Result.Failure(new OAuthProblem.InvalidToken(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatFailureResultWithTokenRejectedMessageIsReturnedWhenTokenIsNotAnAccessToken() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(REQUEST_TOKEN);
        Result failure = new Result.Failure(new OAuthProblem(Problem.TOKEN_REJECTED, TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatFailureResultIsReturnedWhenThereIsAnOAuthProblemDuringMessageValidation() throws Exception {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        doThrow(new OAuthProblemException(OAuth.Problems.SIGNATURE_INVALID)).when(validator).validateMessage(isA(OAuthMessage.class), isA(OAuthAccessor.class));
        Result failure = new Result.Failure(new OAuthProblem(Problem.SIGNATURE_INVALID, "1234"));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatErrorResultIsReturnedWhenThereIsAGeneralExceptionDuringMessageValidation() throws Exception {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        OAuthException toBeThrown = new OAuthException("Unknown problem");
        doThrow(toBeThrown).when(validator).validateMessage(isA(OAuthMessage.class), isA(OAuthAccessor.class));
        Result error = new Result.Error(new OAuthProblem.System(toBeThrown));
        assertThat(authenticator.authenticate(request, response), is(equalTo(error)));
        verify(response).setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatFailureResultWithPermissionDeniedMessageIsReturnedForUserWithValidTokenThatCannotLogIn() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(false);
        Result failure = new Result.Failure(new OAuthProblem.PermissionDenied(USER));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatFailureResultIsReturnedWhenInvalidTokenExceptionIsThrownWhileFetchingToken() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenThrow(new InvalidTokenException("token is invalid"));
        Result failure = new Result.Failure(new OAuthProblem.InvalidToken(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatFailureResultIsReturnedWhenTokenIsExpired() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(EXPIRED_ACCESS_TOKEN);

        Result failure = new Result.Failure(new OAuthProblem.TokenExpired(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }

    @Test
    public void assertThatSuccessIsReturnedForValid2LORequest() {
        setupRequestWithParameters(emptyOAuthToken(rsaConsumerWith2LOParameterMap));
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(USER);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
    }

    @Test
    public void assertThatFailureIsReturnedIf2LOIsOff() {
        setupRequestWithParameters(emptyOAuthToken(rsaConsumerParameterMap));
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result failure = new Result.Failure(new OAuthProblem(Problem.PERMISSION_DENIED));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void assertThatFailureIsReturnedIf2LOExecutingUserCannotLogin() {
        setupRequestWithParameters(emptyOAuthToken(rsaConsumerWith2LOParameterMap));
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(false);
        Result failure = new Result.Failure(new OAuthProblem(Problem.PERMISSION_DENIED, USERNAME));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void assertThatSuccessIsReturnedForValid2LOWhenRequestExecutingUserHasNotBeenSet() {
        setupRequestWithParameters(emptyOAuthToken(rsaConsumerWith2LOButNoExecutingUserParameterMap));
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(null);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
    }

    @Test
    public void assertThatSuccessIsReturnedForValid2LOWhenRequestExecutingUserHasBeenSetToBlank() {
        setupRequestWithParameters(emptyOAuthToken(rsaConsumerWith2LOButBlankExecutingUserParameterMap));
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(null);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
    }

    @Test
    public void assertThatSuccessIsReturnedForValid2LOImpersonationRequest() {
        setupRequestWithParameters(emptyOAuthToken(addImpersonatingUser(rsaConsumerWith2LOImpersonationParameterMap, LONG_USERNAME)));
        when(request.getParameter("xoauth_requestor_id")).thenReturn(LONG_USERNAME);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(userManager.resolve(LONG_USERNAME)).thenReturn(USER_WITH_LONG_USERNAME);
        when(authenticationController.canLogin(USER_WITH_LONG_USERNAME, request)).thenReturn(true);
        Result success = new Result.Success(USER_WITH_LONG_USERNAME);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
        verify(request).setAttribute(OAUTH_CONSUMER_KEY, RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey());
    }

    // OAUTH-262, OAUTH-263: for backward compat with remoteapps 2LO
    @Test
    public void assertThatSuccessIsReturnedForValid2LOImpersonationRequestWithRemoteAppsRequestId() {
        setupRequestWithParameters(emptyOAuthToken(addImpersonatingUser(rsaConsumerWith2LOImpersonationParameterMap, LONG_USERNAME)));
        when(request.getParameter("xoauth_requestor_id")).thenReturn(null);
        when(request.getParameter("user_id")).thenReturn(LONG_USERNAME);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(userManager.resolve(LONG_USERNAME)).thenReturn(USER_WITH_LONG_USERNAME);
        when(authenticationController.canLogin(USER_WITH_LONG_USERNAME, request)).thenReturn(true);
        Result success = new Result.Success(USER_WITH_LONG_USERNAME);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
        verify(request).setAttribute(OAUTH_CONSUMER_KEY, RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey());
    }

    // OAUTH-262, OAUTH-263: for backward compat with remoteapps 2LO
    @Test
    public void defaultRequestIdParameterTakesPrecedenceOverRemoteAppsRequestId() {
        setupRequestWithParameters(emptyOAuthToken(addImpersonatingUser(rsaConsumerWith2LOImpersonationParameterMap, LONG_USERNAME)));
        when(request.getParameter("xoauth_requestor_id")).thenReturn(LONG_USERNAME);
        when(request.getParameter("user_id")).thenReturn(USERNAME);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(userManager.resolve(LONG_USERNAME)).thenReturn(USER_WITH_LONG_USERNAME);
        when(authenticationController.canLogin(USER_WITH_LONG_USERNAME, request)).thenReturn(true);
        Result success = new Result.Success(USER_WITH_LONG_USERNAME);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
        verify(request).setAttribute(OAUTH_CONSUMER_KEY, RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey());
        verify(request).setAttribute(OAUTH_CONSUMER_KEY, RSA_CONSUMER_WITH_2LO_IMPERSONATION.getKey());
    }

    @Test
    public void assertThatFailureIsReturnedForValid2LOImpersonationRequestButImpersonationIsDisabled() {
        setupRequestWithParameters(emptyOAuthToken(addImpersonatingUser(rsaConsumerWith2LOParameterMap, LONG_USERNAME)));
        when(request.getParameter("xoauth_requestor_id")).thenReturn(LONG_USERNAME);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(userManager.resolve(LONG_USERNAME)).thenReturn(USER_WITH_LONG_USERNAME);
        when(authenticationController.canLogin(USER_WITH_LONG_USERNAME, request)).thenReturn(true);
        Result failure = new Result.Failure(new OAuthProblem(Problem.PERMISSION_DENIED, LONG_USERNAME));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(request, never()).setAttribute(anyString(), anyString());
    }

    @Test
    public void assertThatFailureIsReturnedIfUserIn2LOImpersonationCannotLogin() {
        setupRequestWithParameters(emptyOAuthToken(addImpersonatingUser(rsaConsumerWith2LOImpersonationParameterMap, LONG_USERNAME)));
        when(request.getParameter("xoauth_requestor_id")).thenReturn(LONG_USERNAME);
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(userManager.resolve(LONG_USERNAME)).thenReturn(USER_WITH_LONG_USERNAME);
        when(authenticationController.canLogin(USER_WITH_LONG_USERNAME, request)).thenReturn(false);
        Result failure = new Result.Failure(new OAuthProblem(Problem.PERMISSION_DENIED, LONG_USERNAME));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void assertThatSuccessIsReturnedForValidFourYearsOldAccessToken() {
        setupRequestWithParameters(rsaConsumerParameterMap);
        when(store.get(TOKEN)).thenReturn(FOUR_YEARS_OLD_ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(USER);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
    }

    static Matcher<? super Result> equalTo(final Result result) {
        return samePropertyValuesAs(result);
    }

    private static Map<String, String[]> addImpersonatingUser(Map<String, String[]> paramMap, String username) {
        return ImmutableMap.<String, String[]>builder()
                .putAll(paramMap)
                .put("oauth_user_id", new String[]{username})
                .build();
    }

    // empty the oauth_token param to indicate that this is a 2LO request
    private static Map<String, String[]> emptyOAuthToken(Map<String, String[]> paramMap) {
        Map<String, String[]> params = new HashMap<String, String[]>(paramMap);
        params.put("oauth_token", new String[]{""});
        return params;
    }

    private void setupRequestWithParameters(Map<String, String[]> params) {
        when(request.getParameterMap()).thenReturn(params);
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            when(request.getParameter(entry.getKey())).thenReturn(entry.getValue()[0]);
        }
    }
}

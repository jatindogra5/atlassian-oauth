package com.atlassian.oauth.serviceprovider.internal;

import com.google.common.collect.ImmutableList;
import net.oauth.OAuth;
import net.oauth.OAuth.Parameter;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.CombinableMatcher;
import org.junit.Test;

import java.util.Map;

import static net.oauth.OAuth.HMAC_SHA1;
import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static net.oauth.OAuth.OAUTH_NONCE;
import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.OAUTH_TIMESTAMP;
import static net.oauth.OAuth.Problems.OAUTH_PARAMETERS_ABSENT;
import static net.oauth.OAuth.Problems.PARAMETER_ABSENT;
import static net.oauth.OAuth.Problems.SIGNATURE_METHOD_REJECTED;
import static net.oauth.OAuth.RSA_SHA1;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.fail;

public class OAuthValidatorImplTest {
    private final CombinableMatcher.CombinableBothMatcher<Map<? extends String, ?>> isParameterAbsent = both(hasEntry("oauth_problem", (Object) PARAMETER_ABSENT));
    private final Matcher<Map<? extends String, ? extends Object>> isSignatureMethodRejected = hasEntry("oauth_problem", (Object) SIGNATURE_METHOD_REJECTED);

    OAuthValidator validator = new OAuthValidatorImpl();

    @Test
    public void assertThatValidatorRejectsAnyMessagesWithoutASignatureMethod() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(new Parameter(OAUTH_CONSUMER_KEY, "1234")));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException with parameter_absent");
        } catch (OAuthProblemException problem) {
            assertThat(problem, isParameterAbsent.and(missingParameterIs(OAUTH_SIGNATURE_METHOD)));
        }
    }

    @Test
    public void assertThatValidatorRejectsAnyMessagesWithoutAConsumerKey() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(new Parameter(OAUTH_SIGNATURE_METHOD, RSA_SHA1)));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException with parameter_absent");
        } catch (OAuthProblemException problem) {
            assertThat(problem, isParameterAbsent.and(missingParameterIs(OAUTH_CONSUMER_KEY)));
        }
    }

    @Test
    public void assertThatValidatorRejectsAnyMessagesNotUsingRsaSha1AsTheSignatureMethod() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(
                new Parameter(OAUTH_SIGNATURE_METHOD, HMAC_SHA1),
                new Parameter(OAUTH_CONSUMER_KEY, "1234")
        ));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException with signature_method_rejected");
        } catch (OAuthProblemException problem) {
            assertThat(problem, isSignatureMethodRejected);
        }
    }

    @Test
    public void assertThatValidatorRejectsAnyMessagesNotHavingTimestamp() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(
                new Parameter(OAUTH_SIGNATURE_METHOD, RSA_SHA1),
                new Parameter(OAUTH_CONSUMER_KEY, "1234"),
                new Parameter(OAUTH_TIMESTAMP, "1234567")
        ));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException");
        } catch (OAuthProblemException problem) {
            assertThat(problem, isParameterAbsent.and(missingParameterIs(OAUTH_NONCE)));
        }
    }

    @Test
    public void assertThatValidatorRejectsAnyMessagesNotHavingNonce() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(
                new Parameter(OAUTH_SIGNATURE_METHOD, RSA_SHA1),
                new Parameter(OAUTH_CONSUMER_KEY, "1234"),
                new Parameter(OAUTH_NONCE, "nonce1")
        ));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException");
        } catch (OAuthProblemException problem) {
            assertThat(problem, isParameterAbsent.and(missingParameterIs(OAUTH_TIMESTAMP)));
        }
    }

    @Test
    public void assertThatValidatorRejectsVeryOldTimestamp() throws Exception {
        OAuthMessage message = new OAuthMessage(null, null, ImmutableList.of(
                new Parameter(OAUTH_SIGNATURE_METHOD, RSA_SHA1),
                new Parameter(OAUTH_CONSUMER_KEY, "1234"),
                new Parameter(OAUTH_TIMESTAMP, "1"),
                new Parameter(OAUTH_NONCE, "nonce1")
        ));
        try {
            validator.validateMessage(message, null);
            fail("Expected OAuthProblemException");
        } catch (OAuthProblemException problem) {
            assertThat(problem, hasEntry("oauth_problem", (Object) OAuth.Problems.TIMESTAMP_REFUSED));
        }
    }

    private void assertThat(OAuthProblemException e, Matcher<Map<? extends String, ? extends Object>> matcher) {
        MatcherAssert.assertThat(e.getParameters(), matcher);
    }

    private Matcher<Map<? extends String, ? extends Object>> missingParameterIs(String oauthConsumerKey) {
        return hasEntry(OAUTH_PARAMETERS_ABSENT, (Object) oauthConsumerKey);
    }
}

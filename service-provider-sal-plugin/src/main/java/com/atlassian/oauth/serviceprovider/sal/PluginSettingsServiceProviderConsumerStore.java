package com.atlassian.oauth.serviceprovider.sal;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderConsumerStore.Settings.ConsumerProperties;
import com.atlassian.oauth.shared.sal.AbstractSettingsProperties;
import com.atlassian.oauth.shared.sal.HashingLongPropertyKeysPluginSettings;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.oauth.shared.sal.Functions.toDecodedKeys;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;

public class PluginSettingsServiceProviderConsumerStore implements ServiceProviderConsumerStore {
    private final PluginSettingsFactory pluginSettingsFactory;

    public PluginSettingsServiceProviderConsumerStore(PluginSettingsFactory factory) {
        pluginSettingsFactory = checkNotNull(factory, "factory");
    }

    public Consumer get(String consumerKey) {
        checkNotNull(consumerKey, "consumerKey");

        ConsumerProperties props = settings().getConsumerProperties(consumerKey);
        if (props == null) {
            return null;
        }

        try {
            try {
                return Consumer.key(consumerKey)
                        .name(props.getName())
                        .publicKey(props.getPublicKey())
                        .description(props.getDescription())
                        .callback(props.getCallback())
                        .threeLOAllowed(props.getThreeLOAllowed())
                        .twoLOAllowed(props.getTwoLOAllowed())
                        .executingTwoLOUser(props.getExecutingTwoLOUser())
                        .twoLOImpersonationAllowed(props.getTwoLOImpersonationAllowed())
                        .build();
            } catch (NoSuchAlgorithmException e) {
                throw new StoreException(e);
            } catch (InvalidKeySpecException e) {
                throw new StoreException(e);
            }
        } catch (URISyntaxException e) {
            throw new StoreException("callback URI is not valid", e);
        }
    }

    public void put(Consumer consumer) {
        checkNotNull(consumer, "consumer");
        Settings settings = settings();
        settings.addConsumerKey(consumer.getKey());
        settings.putConsumerProperties(consumer.getKey(), new ConsumerProperties(consumer));
    }

    public void remove(String consumerKey) {
        checkNotNull(consumerKey, "consumerKey");
        Settings settings = settings();
        settings.removeConsumerKey(consumerKey);
        settings.removeConsumerProperties(consumerKey);
    }

    public Iterable<Consumer> getAll() {
        return transform(getConsumerKeys(), new Function<String, Consumer>() {
            public Consumer apply(String consumerKey) {
                return get(consumerKey);
            }
        });
    }

    private Iterable<String> getConsumerKeys() {
        return settings().getConsumerKeys();
    }

    private Settings settings() {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }

    static final class Settings {
        private final PluginSettings settings;

        Settings(PluginSettings settings) {
            this.settings = new PrefixingPluginSettings(new HashingLongPropertyKeysPluginSettings(settings), ServiceProviderConsumerStore.class.getName());
        }

        ConsumerProperties getConsumerProperties(String consumerKey) {
            Properties props = (Properties) settings.get(consumerSettingKey(consumerKey));
            if (props == null) {
                return null;
            }
            return new ConsumerProperties(props);
        }

        void putConsumerProperties(String key, AbstractSettingsProperties consumerProperties) {
            // OAUTH-232:  the prefixed key could end up getting hashed, but it's not necessary to embed another
            // copy of the consumer key in the properties here, because we can always look at the main list of
            // consumer keys (which is stored under an invariant key that won't get hashed).
            settings.put(consumerSettingKey(key), consumerProperties.asProperties());
        }

        void removeConsumerProperties(String key) {
            settings.remove(consumerSettingKey(key));
        }

        private String consumerSettingKey(String key) {
            return "consumer." + key;
        }

        Set<String> getConsumerKeys() {
            String encodedKeys = (String) settings.get(Keys.CONSUMER_KEYS);
            if (encodedKeys == null) {
                return ImmutableSet.of();
            }
            return ImmutableSet.copyOf(transform(asList(encodedKeys.split("/")), toDecodedKeys()));
        }

        void putConsumerKeys(Iterable<String> keys) {
            if (isEmpty(keys)) {
                settings.remove(Keys.CONSUMER_KEYS);
            } else {
                settings.put(Keys.CONSUMER_KEYS, Joiner.on("/").join(transform(keys, toEncodedKeys())));
            }
        }

        void addConsumerKey(String key) {
            putConsumerKeys(Sets.union(ImmutableSet.of(key), getConsumerKeys()));
        }

        void removeConsumerKey(String key) {
            putConsumerKeys(Sets.filter(getConsumerKeys(), not(equalTo(key))));
        }

        static final class ConsumerProperties extends AbstractSettingsProperties {
            static final String PUBLIC_KEY = "publicKey";
            static final String CALLBACK = "callback";
            static final String DESCRIPTION = "description";
            static final String NAME = "name";

            static final String THREE_LO_ALLOWED = "threeLOAllowed";
            static final String TWO_LO_ALLOWED = "twoLOAllowed";
            static final String EXECUTING_TWO_LO_USER = "executingTwoLOUser";
            static final String TWO_LO_IMPERSONATION_ALLOWED = "twoLOImpersonationAllowed";

            ConsumerProperties(Consumer consumer) {
                super();
                putName(consumer.getName());
                putPublicKey(consumer.getPublicKey());
                putDescription(consumer.getDescription());
                putCallback(consumer.getCallback());
                putThreeLOAllowed(consumer.getThreeLOAllowed());
                putTwoLOAllowed(consumer.getTwoLOAllowed());
                putExecutingTwoLOUser(consumer.getExecutingTwoLOUser());
                putTwoLOImpersonationAllowed(consumer.getTwoLOImpersonationAllowed());
            }

            ConsumerProperties(Properties properties) {
                super(properties);
            }

            String getName() {
                return get(NAME);
            }

            void putName(String name) {
                put(NAME, name);
            }

            PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
                return RSAKeys.fromPemEncodingToPublicKey(get(PUBLIC_KEY));
            }

            void putPublicKey(PublicKey publicKey) {
                put(PUBLIC_KEY, RSAKeys.toPemEncoding(publicKey));
            }

            String getDescription() {
                return get(DESCRIPTION);
            }

            void putDescription(String description) {
                if (description == null) {
                    return;
                }
                put(DESCRIPTION, description);
            }

            URI getCallback() throws URISyntaxException {
                String callback = get(CALLBACK);
                if (callback == null) {
                    return null;
                }
                return new URI(callback);
            }

            void putCallback(URI callback) {
                if (callback == null) {
                    return;
                }
                put(CALLBACK, callback.toString());
            }

            boolean getThreeLOAllowed() {
                String isAllowed = get(THREE_LO_ALLOWED);

                // for backward compatibility, not having this defined means it's on.
                if (isAllowed == null) {
                    return true;
                }

                return Boolean.parseBoolean(isAllowed);
            }

            void putThreeLOAllowed(boolean threeLOAllowed) {
                put(THREE_LO_ALLOWED, Boolean.toString(threeLOAllowed));
            }

            boolean getTwoLOAllowed() {
                String isAllowed = get(TWO_LO_ALLOWED);
                if (isAllowed == null) {
                    return false;
                }

                return Boolean.parseBoolean(isAllowed);
            }

            void putTwoLOAllowed(boolean twoLOAllowed) {
                put(TWO_LO_ALLOWED, Boolean.toString(twoLOAllowed));
            }

            String getExecutingTwoLOUser() {
                return get(EXECUTING_TWO_LO_USER);
            }

            void putExecutingTwoLOUser(String executingTwoLOUser) {
                if (executingTwoLOUser == null) {
                    return;
                }
                put(EXECUTING_TWO_LO_USER, executingTwoLOUser);
            }

            boolean getTwoLOImpersonationAllowed() {
                String isAllowed = get(TWO_LO_IMPERSONATION_ALLOWED);
                if (isAllowed == null) {
                    return false;
                }

                return Boolean.parseBoolean(isAllowed);
            }

            void putTwoLOImpersonationAllowed(boolean twoLOImpersonationAllowed) {
                put(TWO_LO_IMPERSONATION_ALLOWED, Boolean.toString(twoLOImpersonationAllowed));
            }
        }

        static final class Keys {
            static final String CONSUMER_KEYS = "allConsumerKeys";
        }
    }
}

package it.com.atlassian.oauth;

import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthServiceProvider;
import net.oauth.client.OAuthClient;
import net.oauth.client.httpclient4.HttpClient4;

import java.io.IOException;

import static junit.framework.Assert.assertTrue;
import static net.oauth.signature.RSA_SHA1.PRIVATE_KEY;

public abstract class OAuthTestBase {
    /**
     * WebDriver representation of RefApp
     *
     * @since 1.9.0
     */
    protected final RefappTestedProduct PRODUCT = TestedProductFactory.create(RefappTestedProduct.class, new RefappInstance(), null);

    protected static final String BASE_URL = System.getProperty("baseurl") != null
            ? System.getProperty("baseurl") : "http://localhost:3200/oauth";

    protected static final String SERVLET_BASE_URL = BASE_URL + "/plugins/servlet";
    protected static final String REQUEST_TOKEN_URL = SERVLET_BASE_URL + "/oauth/request-token";
    protected static final String AUTHORIZE_URL = SERVLET_BASE_URL + "/oauth/authorize";
    protected static final String ACCESS_TOKEN_URL = SERVLET_BASE_URL + "/oauth/access-token";
    protected static final String LOGIN_URL = SERVLET_BASE_URL + "/login";
    protected static final String WHOAMI_URL = SERVLET_BASE_URL + "/whoami";
    protected static final String STRICT_SECURITY_URL = SERVLET_BASE_URL + "/oauth/integration-test/strict-security";

    protected static final String CALLBACK_URL = "http://localhost:8080/consumer/oauthcallback";
    protected static final String CONSUMER_KEY = "hardcoded-consumer";

    protected static final String NOCALLBACK_CONSUMER_KEY = "hardcoded-nocallback";

    protected static final String CONSUMER_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFkPMZQaTqsSXI+bSI65rSVaDzic6WFA3WCZMVMi7lYXJAUdkXo4DgdfvEBO21Bno3bXIoxqS411G8S53I39yhSp7z2vcB76uQQifi0LEaklZfbTnFUXcKCyfwgKPp0tQVA+JZei6hnscbSw8qEItdc69ReZ6SK+3LHhvFUUP1nLhJDsgdPHRXSllgZzqvWAXQupGYZVANpBJuK+KAfiaVXCgA71N9xx/5XTSFi5K+e1T4HVnKAzDasAUt7Mmad+1PE+56Gpa73FLk1Ww+xaAEvss6LehjyWHM5iNswoNYzrNS2k6ZYkDnZxUlbrPDELETbz/n3YgBHGUlyrXi2PBjAgMBAAECggEAAtMctqq6meRofuQbEa4Uq5cv0uuQeZLV086VPMNX6k2nXYYODYl36T2mmNndMC5khvBYpn6Ykk/5yjBmlB2nQOMZPLFPwMZVdJ2Nhm+naJLZC0o7fje49PrN2mFsdoZeI+LHVLIrgoILpLdBAz/zTiW+RvLvMnXQU4wdp4eO6i8J/Jwh0AY8rWsAGkk1mdZDwklPZZiwR3z+DDsDwPxFs8z6cE5rWJd2c/fhAQrHwOXyrQPsGyLHTOqS3BkjtEZrKRUlfdgV76VlThwrE5pAWuO0GPyfK/XCklwcNS1a5XxCOq3uUogWRhCsqUX6pYfAVS6xzX56MGDndQVlp7U5uQKBgQDyTDwhsNTWlmr++FyYrc6liSF9NEMBNDubrfLJH1kaOp590bE8fu3BG0UlkVcueUr05e33Kx1DMSFW72lR4dht1jruWsbFp6LlT3SUtyW2kcSet3fC8gySs2r6NncsZ2XFPoxTkalKpQ1atGoBe3XIKeT8RDZtgoLztQy7/7yANQKBgQDQvSHEKS5SttoFFf4YkUh2QmNX5m7XaDlTLB/3xjnlz8NWOweK1aVysb4t2Tct/SR4ZZ/qZDBlaaj4X9h9nlxxIMoXEyX6Ilc4tyCWBXxn6HFMSa/Rrq662Vzz228cPvW2XGOQWdj7IqwKO9cXgJkI5W84YtMtYrTPLDSjhfpxNwKBgGVCoPq/iSOpN0wZhbE1KiCaP8mwlrQhHSxBtS6CkF1a1DPm97g9n6VNfUdnB1Vf0YipsxrSBOe416MaaRyUUzwMBRLqExo1pelJnIIuTG+RWeeu6zkoqUKCAxpQuttu1uRo8IJYZLTSZ9NZhNfbveyKPa2D4G9B1PJ+3rSO+ztlAoGAZNRHQEMILkpHLBfAgsuC7iUJacdUmVauAiAZXQ1yoDDo0Xl4HjcvUSTMkccQIXXbLREh2w4EVqhgR4G8yIk7bCYDmHvWZ2o5KZtD8VO7EVI1kD0z4Zx4qKcggGbp2AINnMYqDetopX7NDbB0KNUklyiEvf72tUCtyDk5QBgSrqcCgYEAnlg3ByRd/qTFz/darZi9ehT68Cq0CS7/B9YvfnF7YKTAv6J2Hd/i9jGKcc27x6IMi0vf7zrqCyTMq56omiLdu941oWfsOnwffWRBInvrUWTj6yGHOYUtg2z4xESUoFYDeWwe/vX6TugL3oXSX3Sy3KWGlJhn/OmsN2fgajHRip0=";

    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING = "bb6dd1391ce33b5bd3ecad1175139a39";
    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_AUTHORIZING = "29c3005cc5fbe5d431f27b29d6191ea3";

    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING = "iezied5IEeh0IoquuGh9riexUenei4Ai";
    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_WITH_NO_CALLBACK_FOR_AUTHORIZING = "xei1kohXEepheed3Hemie7AhpoiG2cum";

    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING = "RiZie2UaooXee5siJi6gee0tmeeBe0cu";
    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_DENYING = "ew0kaiK1Eetekee2Ahjah2hoAif5eu9P";

    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING = "Ga9zoo0Ger0oa0IuNaeShoh4eiShae6a";
    protected static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_WITH_NO_CALLBACK_FOR_DENYING = "Zijae1XuoT5AYooneingi4NoXiw0uvee";

    protected static final String SPARE_NON_AUTHORIZED_REQUEST_TOKEN = "cc7ee2402df44c6ce4fdbe2286240b40";
    protected static final String SPARE_NON_AUTHORIZED_REQUEST_TOKEN_SECRET = "30d4116dd6acf6e542a38c30e7202fb4";

    protected static final String AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING = "5c09d8d4e50065eb49a05200035bd780";
    protected static final String AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING = "870abbc4847d9b5790cff56a2e9b8279";
    protected static final String AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING = "cu3Aechunoo1CeluKiK1Tielphooci7I";

    protected static final String AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING_WITH_SESSION = "lz6wwY3taMIjzyZK2iC7PvRWNu0LR3Co";
    protected static final String AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING_WITH_SESSION = "WiRmzLV5IRG26kdMWEqrxVNL5LU1wofv";
    protected static final String AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING_WITH_SESSION = "ADgmcc";

    protected static final String SPARE_AUTHORIZED_REQUEST_TOKEN = "6b10e9e5f61176fc50b16311146ce891";
    protected static final String SPARE_AUTHORIZED_REQUEST_TOKEN_SECRET = "981bccd5958e0c6801daa67b3f0c9380";
    protected static final String SPARE_AUTHORIZED_REQUEST_TOKEN_VERIFIER = "deShay0zai2YeenishooTa7iaB9suph1";

    protected static final String ACCESS_TOKEN = "71b5607f60c0aae6161ce251dd55e8ed";
    protected static final String ACCESS_TOKEN_SECRET = "160881ffbe3c4ff0f6a1ba9078e92e83";

    protected static final String ACCESS_TOKEN_ON_NON_3LO_CONSUMER = "91b5607f60c0aae6161ce251dd55e8ed";
    protected static final String ACCESS_TOKEN_SECRET_ON_NON_3LO_CONSUMER = "960881ffbe3c4ff0f6a1ba9078e92e83";

    protected static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING = "Quie9Tooico3ahShpagh0Voodoh1Phah";
    protected static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING = "iqu3Ti8siet7uoShcoonaeT6zu5woh3H";
    protected static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING = "eep2eeTaich5aiQuroos6IegIer2eife";
    protected static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING = "ieXiiN8jIec8ohbioopaqu4Athei2eiT";
    protected static final String AUTHORIZED_V1_REQUEST_TOKEN_FOR_SWAPPING = "laim9XailocaZoh3Aeph2aekweesi5Ie";

    protected static final String RENEWABLE_ACCESS_TOKEN = "oeRah6xePai6cou3Phiequ2atoo0OhJu";
    protected static final String RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE = "Bohro1ziaefaJ1FiAifaKai8Phah5ahH";

    protected static final String NON_RENEWABLE_ACCESS_TOKEN = "loP1shofiDoo5eeGiequ8oZurei8Ahch";
    protected static final String NON_RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE = "Ohs5ux1kzohJu4Eeaiv0no3Ujoowae8F";

    protected static final String CALLBACK_URI = "http://consumer/callback";

    protected final OAuthClient createClient() {
        return new OAuthClient(new HttpClient4());
    }

    protected final OAuthAccessor createAccessor() throws IOException {
        return createAccessor(CONSUMER_KEY);
    }

    protected final OAuthAccessor createAccessor(String consumerKey) throws IOException {
        OAuthServiceProvider serviceProvider = new OAuthServiceProvider(REQUEST_TOKEN_URL, AUTHORIZE_URL, ACCESS_TOKEN_URL);

        OAuthConsumer consumer = new OAuthConsumer(CALLBACK_URL, consumerKey, null, serviceProvider);
        consumer.setProperty(PRIVATE_KEY, CONSUMER_PRIVATE_KEY);
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);

        return new OAuthAccessor(consumer);
    }

    /**
     * WebDriver style login
     *
     * @since 1.9.0
     */
    protected final void login(User user) throws Exception {
        login(user.username, user.password, PRODUCT);
    }

    /**
     * WebDriver style login
     *
     * @since 1.9.0
     */
    public static void login(String username, String password, RefappTestedProduct product) {
        RefappLoginPage loginPage = product.visit(RefappLoginPage.class);
        RefappHomePage homePage = loginPage.login(username, password, RefappHomePage.class);
        assertTrue("Must have logged in.", homePage.getHeader().isLoggedIn());
    }

    protected void logout(RefappTestedProduct product) {
        product.visit(RefappHomePage.class);
        product.getTester().getDriver().manage().deleteAllCookies();
    }

    class RefappInstance implements ProductInstance {

        @Override
        public String getBaseUrl() {
            return BASE_URL;
        }

        @Override
        public int getHttpPort() {
            // TODO dynamically work out from BaseUrl
            return 3200;
        }

        @Override
        public String getContextPath() {
            // TODO dynamically work out from BaseUrl
            return "oauth";
        }

        @Override
        public String getInstanceId() {
            return "refapp";
        }
    }

    protected enum User {
        FRED("fred", "fred");

        public final String username;
        public final String password;

        private User(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }
}
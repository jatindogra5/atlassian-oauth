package it.com.atlassian.oauth;

import com.google.common.collect.ImmutableMap;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.client.OAuthClient;
import org.junit.Ignore;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static net.oauth.OAuth.Problems.CONSUMER_KEY_UNKNOWN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;

public class OAuth2LOTest extends OAuthTestBase {
    private static final String CONSUMER_DOES_NOT_EXIST_KEY = "consumer_does_not_exist_key";
    private static final String CONSUMER_2LO_KEY = "hardcoded-2lo-consumer";
    private static final String CONSUMER_2LO_ONLY_KEY = "hardcoded-2lo-only-consumer";
    private static final String CONSUMER_2LO_WITH_BAD_EXECUTING_USER_KEY = "hardcoded-2lo-consumer-bad-executing-user";
    private static final String CONSUMER_2LO_WITH_NULL_EXECUTING_USER_KEY = "hardcoded-2lo-consumer-null-executing-user";
    private static final String CONSUMER_2LO_IMPERSONATION_KEY = "hardcoded-2lo-impersonation-consumer";
    private static final String CONSUMER_2LO_IMPERSONATION_ONLY_KEY = "hardcoded-2lo-impersonation-only-consumer";

    @Test
    public void assertThatConsumerCannotAccessResourceIfTheConsumerKeyDoesNotExist() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_DOES_NOT_EXIST_KEY);
        OAuthClient client = createClient();

        try {
            client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
            fail("OAuthProblemException expected");
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(equalTo(CONSUMER_KEY_UNKNOWN)));
        }
    }

    @Test
    public void assertThatConsumerCanAccessResourceThrough2LO() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthClient client = createClient();

        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
        assertThat(response.readBodyAsString(), is(equalTo("barney")));
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }

    @Test
    public void assertThatConsumerCanAccessResourceThrough2LOUsingAuthorizationHeader() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthMessage message = accessor.newRequestMessage("GET", STRICT_SECURITY_URL,
                ImmutableMap.of("oauth_token", "").entrySet());

        HttpURLConnection conn = createConnection(message);
        assertThat(conn.getResponseCode(), is(HttpServletResponse.SC_OK));
    }

    @Test
    public void assertThatConsumerCannotAccessResourceThrough2LOUsingIncompleteAuthorizationHeader() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthMessage message = accessor.newRequestMessage("GET", STRICT_SECURITY_URL,
                ImmutableMap.of().entrySet());

        HttpURLConnection conn = createConnection(message);
        assertThat(conn.getResponseCode(), is(HttpServletResponse.SC_UNAUTHORIZED));
    }

    private HttpURLConnection createConnection(OAuthMessage message) throws IOException {
        URL url = new URL(STRICT_SECURITY_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", message.getAuthorizationHeader(null));
        conn.connect();
        return conn;
    }


    @Test
    public void assertThatConsumerCannotAccessResourceThrough2LOIfItIsNotEnabled() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_KEY);
        OAuthClient client = createClient();

        try {
            client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
            fail("OAuthProblemException expected");
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(nullValue()));
        }
    }

    @Test(expected = OAuthProblemException.class)
    public void assertThatConsumerWithWrongSignatureCannotAccessResourceThrough2LO() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthClient client = createClient();
        client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "", "oauth_signature", "wrong_signature").entrySet());
    }

    @Test(expected = OAuthProblemException.class)
    public void assertThatConsumerCannotAccessResourceThrough2LOIfExecutingUserDoesNotExist() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_WITH_BAD_EXECUTING_USER_KEY);
        OAuthClient client = createClient();
        client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
    }

    @Ignore("OAUTH-281. Need to test against REST resource not servlet, awaiting @VerifiedAllowed annotation")
    @Test
    public void assertThatConsumerCanAccessResourceThrough2LOIfExecutingUserIsNull() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_WITH_NULL_EXECUTING_USER_KEY);
        OAuthClient client = createClient();

        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
        assertThat(response.readBodyAsString(), is(equalTo(null)));
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }

    @Test
    public void assertThatConsumerCanAccessResourceThrough2LOImpersonation() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_IMPERSONATION_KEY);
        OAuthClient client = createClient();

        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "", "xoauth_requestor_id", "betty").entrySet());
        assertThat(response.readBodyAsString(), is(equalTo("betty")));
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }

    @Test
    public void assertThatConsumerCannotAccessResourceThrough2LOImpersonationIfImpersonationIsNotEnabled() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthClient client = createClient();

        try {
            client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "", "xoauth_requestor_id", "betty").entrySet());
            fail("OAuthProblemException expected");
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(nullValue()));
        }
    }

    @Test
    public void assertThatConsumerCannotAccessResourceThrough2LOImpersonationIfUserIsInvalid() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_KEY);
        OAuthClient client = createClient();

        try {
            client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "", "xoauth_requestor_id", "does_not_exist").entrySet());
            fail("OAuthProblemException expected");
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(nullValue()));
        }
    }

    @Test
    public void assertThat2LOImpersonationCanWorkCompletelyIndependentOf2LO() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_IMPERSONATION_ONLY_KEY);
        OAuthClient client = createClient();

        // This is 2LO with Impersonation so it must work.
        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "", "xoauth_requestor_id", "betty").entrySet());
        assertThat(response.readBodyAsString(), is(equalTo("betty")));

        // Now try standard 2LO, it must fail.
        try {
            client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
            fail("OAuthProblemException expected");
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(nullValue()));
        }
    }

    @Test
    public void assertThatConsumerCanAccessResourceThrough2LOOnlyConsumerMeaning2LOIsIndependentOf3LO() throws Exception {
        OAuthAccessor accessor = createAccessor(CONSUMER_2LO_ONLY_KEY);
        OAuthClient client = createClient();

        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, ImmutableMap.of("oauth_token", "").entrySet());
        assertThat(response.readBodyAsString(), is(equalTo("barney")));
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }
}

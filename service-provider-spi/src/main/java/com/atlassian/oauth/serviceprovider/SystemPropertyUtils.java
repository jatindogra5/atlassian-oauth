package com.atlassian.oauth.serviceprovider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemPropertyUtils {
    private static final Logger LOG = LoggerFactory.getLogger(SystemPropertyUtils.class);

    /**
     * @param propertyName a JVM system property
     * @param defaultValue the value to return if the propertyName is not defined or not a valid String representation
     *                     of a long
     * @return the parsed long value of the system property value, or the defaultValue if the system property is
     * undefined or invalid
     */
    static long parsePositiveLongFromSystemProperty(final String propertyName, final long defaultValue) {
        final String propertyValue = System.getProperty(propertyName);
        if (propertyValue != null && !propertyValue.trim().isEmpty()) {
            try {
                final long longValue = Long.parseLong(propertyValue);
                if (longValue >= 0) {
                    return longValue;
                } else {
                    LOG.warn(String.format("Value of system property '%s' is negative ('%s') defaulting to %s",
                            propertyName, longValue, defaultValue));
                }
            } catch (NumberFormatException e) {
                LOG.warn(String.format(
                        "Failed to parse long value from system property '%s' (was: '%s'), defaulting to %s",
                        propertyName, propertyValue, defaultValue), e);
            }
        }
        return defaultValue;
    }
}
